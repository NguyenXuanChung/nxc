const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const fs = require('fs');
const cors = require('cors');

const app = express();

app.use(fileUpload({ createParentPath: true, limits: { fileSize: 50 * 1024 * 1024 }, abortOnLimit: true }));
app.use(cors());

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get('/', (req, res) => {
	res.json({ message: 'Welcome to BiHRP-Admin API application.' });
});

// Quản trị hệ thống
require('./app/routes/portal_account.routes')(app);
require('./app/routes/auth.routes')(app);
require('./app/routes/portal_nhomQuyen.routes')(app);
require('./app/routes/sys_attachFiles.routes')(app);
require('./app/routes/sys_admin.routes')(app);
require('./app/routes/sys_comments.routes')(app);
require('./app/routes/sys_workflow.routes')(app);
// Quản lý cán bộ
require('./app/routes/donVi.routes')(app);
// Danh mục
require('./app/routes/dm.routes')(app);
// MDM
require('./app/routes/mdm.routes')(app);
require('./app/routes/mdm_thongBao.routes')(app);

// //if we are here then the specified request is not found
// app.use((req,res,next)=> {
//   const err = new Error("Not Found");
//   err.status = 404;
//   next(err);
// });

// //all other requests are not implemented.
// app.use((err,req, res, next) => {
//  res.status(err.status || 501);
//  res.json({
//      error: {
//          code: err.status || 501,
//          message: err.message
//      }
//  });
// });

// SSL
var privateKey = fs.readFileSync('ssl/server.key', 'utf8');
var certificate = fs.readFileSync('ssl/server.crt', 'utf8');
var credentials = { key: privateKey, cert: certificate };

// var server = require('https').createServer(credentials, app);
var server = require('http').createServer(app);
// set port, listen for requests
const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}.`);
});


// const trackRoute = express.Router();
// const multer = require('multer');

// const mongodb = require('mongodb');
// const MongoClient = require('mongodb').MongoClient;
// const ObjectID = require('mongodb').ObjectID;

// /**
//  * NodeJS Module dependencies.
//  */
// const { Readable } = require('stream');


// app.use('/tracks', trackRoute);

// /**
//  * Connect Mongo Driver to MongoDB.
//  */
// let db;
// MongoClient.connect('mongodb://localhost/trackDB', (err, database) => {
// 	console.log('1234');

//   if (err) {
//     console.log('MongoDB Connection Error. Please make sure that MongoDB is running.');
//     process.exit(1);
//   }
//   db = database;
// });
// trackRoute.get('/:trackID', (req, res) => {
// 	console.log('123');
//   try {
//     var trackID = new ObjectID(req.params.trackID);
//   } catch(err) {
//     return res.status(400).json({ message: "Invalid trackID in URL parameter. Must be a single String of 12 bytes or a string of 24 hex characters" }); 
//   }
//   res.set('content-type', 'audio/mp3');
//   res.set('accept-ranges', 'bytes');

//   let bucket = new mongodb.GridFSBucket(db, {
//     bucketName: 'tracks'
//   });

//   let downloadStream = bucket.openDownloadStream(trackID);

//   downloadStream.on('data', (chunk) => {
//     res.write(chunk);
//   });

//   downloadStream.on('error', () => {
//     res.sendStatus(404);
//   });

//   downloadStream.on('end', () => {
//     res.end();
//   });
// });

// /**
//  * POST /tracks
//  */
// trackRoute.post('/', (req, res) => {
// 	console.log(req.body);

//   const storage = multer.memoryStorage()
//   const upload = multer({ storage: storage, limits: { fields: 1, fileSize: 6000000, files: 1, parts: 2 }});
//   upload.single('track')(req, res, (err) => {
//     if (err) {
//       return res.status(400).json({ message: "Upload Request Validation Failed" });
//     } else if(!req.body.name) {
//       return res.status(400).json({ message: "No track name in request body" });
//     }
    
//     let trackName = req.body.name;
    
//     // Covert buffer to Readable Stream
//     const readableTrackStream = new Readable();
//     readableTrackStream.push(req.file.buffer);
//     readableTrackStream.push(null);

//     let bucket = new mongodb.GridFSBucket(db, {
//       bucketName: 'tracks'
//     });

//     let uploadStream = bucket.openUploadStream(trackName);
//     let id = uploadStream.id;
//     readableTrackStream.pipe(uploadStream);

//     uploadStream.on('error', () => {
//       return res.status(500).json({ message: "Error uploading file" });
//     });

//     uploadStream.on('finish', () => {
//       return res.status(201).json({ message: "File uploaded successfully, stored under Mongo ObjectID: " + id });
//     });
//   });
// });

// const path = require('path')
// var http = require('http')
// app.get('/audio', function(request, response){
// 	var inputStream = fs.open('uploads/sample.mp3')
// 	inputStream.pipe(response);
// })
// app.get('/video', function (req, response) {
// 	console.log('123');
// 	const path = 'uploads/sample.mp3'
// 	// var filePath = 'http://171.244.27.196:8080/hmdm/files/123.mp3';
// 	var filePath = 'uploads/sample.mp3'
// 	var stat = fs.statSync(filePath);

// 	response.writeHead(200, {
// 			'Content-Type': 'audio/mp3',
// 			'Content-Length': stat.size,
// 			'accept-ranges': 'bytes'
// 	});
// 	var readStream = fs.createReadStream(filePath);
// 	// readStream.on('open', (chunk) =>{
// 	// 	console.log(chunk);
// 	// 	response.write(chunk);
// 	// })
// 	readStream.pipe(response);



// 	// const stat = fs.statSync(path)
// 	// const fileSize = stat.size
// 	// const range = req.headers.range

// 	// if (range) {
// 	// 	const parts = range.replace(/bytes=/, "").split("-")
// 	// 	const start = parseInt(parts[0], 10)
// 	// 	const end = parts[1]
// 	// 		? parseInt(parts[1], 10)
// 	// 		: fileSize - 1

// 	// 	const chunksize = (end - start) + 1
// 	// 	const file = fs.createReadStream(path, { start, end })
// 	// 	const head = {
// 	// 		'Content-Range': `bytes ${start}-${end}/${fileSize}`,
// 	// 		'Accept-Ranges': 'bytes',
// 	// 		'Content-Length': chunksize,
// 	// 		'Content-Type': 'audio/mpeg',
// 	// 	}

// 	// 	res.writeHead(206, head)
// 	// 	file.pipe(res)
// 	// } else {
// 	// 	const head = {
// 	// 		'Content-Length': fileSize,
// 	// 		'Content-Type': 'audio/mpeg',
// 	// 	}
// 	// 	res.writeHead(200, head)
// 	// 	fs.createReadStream(path).pipe(res)
// 	// }
// })