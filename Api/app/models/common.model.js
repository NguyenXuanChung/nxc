const sql = require('./db');
const moment = require('moment-timezone');
const config = require('../config/db.config');
const StringBuilder = require('string-builder');
const fs = require('fs');
const path = require('path');
const util = require('util');

// constructor
const Common = function () { };

Common.get = async (body, result) => {
	let conn;

	try {
		var where = [],
			query = '',
			data = body.data,
			values = [];

		for (var key in data) {
			if (data[key]) {
				where.push(`${key} = ?`);
				values.push(data[key]);
			}
		}

		// Nếu không truyền status thì set là 1
		if (!data.status) {
			where.push(`status = ?`);
			values.push(1);
		}

		if (where != '') {
			query = `SELECT * FROM ${body.table} WHERE ${where.join(' AND ')}`;
		} else {
			query = `SELECT * FROM ${body.table}`;
		}

		conn = await sql.getConnection();
		const res = await conn.query(query, values);
		if (res.length) {
			console.log(`found: `, res.length);
			result(null, res);
			return;
		}
		// not found portal_account with the id
		result({ kind: 'not_found' }, null);
	} catch (err) {
		console.log('error: ', err);
		result(err, null);
	} finally {
		if (conn) conn.release(); //release to pool
	}
};

// Thực hiện tìm kiếm với câu lệnh phức tạp gồm inner join, left join, where với operator
Common.getAdvance = async (body, result) => {
	let conn;
	let query = new StringBuilder('SELECT ');

	try {
		var values = [],
			params = [];

		// SELECT
		if (body.select) {
			for (var key in body.select) {
				if (body.select[key] != '') {
					values.push(`${key} AS ${body.select[key]}`);
				} else {
					values.push(`${key}`);
				}
			}
			query.append(values.join(', '));
		}

		// SELECT NGƯỢC
		if (body.selectNguoc) {
			for (var key in body.selectNguoc) {
				if (body.selectNguoc[key] != '') {
					values.push(`${body.selectNguoc[key]} AS ${key}`);
				} else {
					values.push(`${key}`);
				}
			}
			query.append(values.join(', '));
		}

		// FROM
		if (body.from) {
			query.appendLine(`FROM ${body.from}`);
		}

		//INNER JOIN
		if (body.inner_join) {
			for (var key in body.inner_join) {
				query.appendLine(`INNER JOIN ${key} ON ${body.inner_join[key]}`);
			}
		}

		// LEFT JOIN
		if (body.left_join) {
			for (var key in body.left_join) {
				query.appendLine(`LEFT JOIN ${key} ON ${body.left_join[key]}`);
			}
		}

		// WHERE
		if (body.where) {
			values = [];
			for (var key in body.where) {
				if (body.where[key].value) {
					if (body.where[key].operator === 'IN') {
						values.push(`${key} ${body.where[key].operator} (?)`);
						params.push(body.where[key].value.replace(/(^[,\s]+)|([,\s]+$)/g, ''));
					} else if (body.where[key].operator === 'LIKE') {
						values.push(`${key} ${body.where[key].operator} ?`);
						params.push(`%${body.where[key].value}%`);
					} else if (body.where[key].operator === 'BETWEEN') {
						values.push(`${key} ${body.where[key].operator} ? AND ?`);
						params.push(`%${body.where[key].from}%`);
						params.push(`%${body.where[key].to}%`);
					} else if (key.includes('FIND_IN_SET(')) {
						if (body.where[key].list) {
							values.push(`${key}, ?) ${body.where[key].operator} ?`);
							params.push(body.where[key].list);
							params.push(body.where[key].value);
						}
					} else {
						values.push(`${key} ${body.where[key].operator} ?`);
						params.push(body.where[key].value);
					}
				}
			}
			query.appendLine(`WHERE ${values.join(' AND ')}`);
		}

		// WHERE GROUP
		if (body.whereGroup) {
			values = [];
			for (var groupKey in body.whereGroup) {
				if (body.whereGroup[groupKey]) {
					let subValues = [];
					body.whereGroup[groupKey].forEach((element) => {
						for (var key in element) {
							if (element[key].operator === 'IN') {
								subValues.push(
									`${key} ${element[key].operator} (${element[key].value.replace(
										/(^[,\s]+)|([,\s]+$)/g,
										''
									)})`
								);
							} else if (element[key].operator === 'LIKE') {
								subValues.push(`${key} ${element[key].operator} ?`);
								params.push(`%${element[key].value}%`);
							} else if (element[key].operator === 'BETWEEN') {
								subValues.push(`${key} ${element[key].operator} ? AND ?`);
								params.push(`${element[key].from}`);
								params.push(`${element[key].to}`);
							} else if (key.includes('FIND_IN_SET(')) {
								if (element[key].list) {
									subValues.push(
										`${key}, '${element[key].list}') ${element[key].operator} ${element[key].value}`
									);
								}
							} else {
								subValues.push(`${key} ${element[key].operator} ?`);
								params.push(element[key].value);
							}
						}
					});
					values.push(`(${subValues.join(' OR ')})`);
				}
			}
			if (values.length > 0) query.appendLine(`WHERE ${values.join(' AND ')}`);
		}

		// ORDER BY
		if (body.orderBy) {
			query.appendLine(`ORDER BY ${body.orderBy}`);
		}

		// GROUP BY
		if (body.groupBy) {
			query.appendLine(`GROUP BY ${body.groupBy}`);
		}
		// console.log(query.toString(), params);
		conn = await sql.getConnection();
		const res = await conn.query(query.toString(), params);
		if (res.length) {
			console.log(`found: `, res.length);
			result(null, res);
			return;
		}
		// not found data
		result({ kind: 'not_found' }, null);
	} catch (err) {
		console.log('error: ', err);
		result(err, null);
	} finally {
		if (conn) conn.release(); //release to pool
	}
};

Common.create = async (body, result) => {
	let conn;

	try {
		var insert = ['status'],
			value = [1],
			values = [],
			data = body.data;

		for (var key in data) {
			insert.push(key);
			value.push('?');
			values.push(data[key]);
		}

		var query = `INSERT INTO ${body.table} (${insert.join(', ')}) VALUES (${value.join(', ')})`;

		conn = await sql.getConnection();
		const res = await conn.query(query, values);
		console.log(`created ${body.table}: `, { id: res.insertId, ...body.data });
		result(null, { id: res.insertId, ...body.data });
	} catch (err) {
		console.log('error: ', err);
		result(err, null);
	} finally {
		if (conn) conn.release(); //release to pool
	}
};

Common.update = async (body, result) => {
	let conn;

	try {
		var set = [],
			values = [],
			where = [],
			data = body.data,
			condition = body.condition;

		for (var key in data) {
			set.push(`${key} = ?`);
			values.push(data[key]);
		}

		for (var key in condition) {
			where.push(`${key} = ?`);
			values.push(condition[key]);
		}

		var query = `UPDATE ${body.table} SET ${set.join(', ')} WHERE ${where.join(' AND ')}`;

		conn = await sql.getConnection();
		const res = await conn.query(query, values);
		if (res.affectedRows == 0) {
			// not found data with the id
			result({ kind: 'not_found' }, null);
			return;
		}
		console.log(`updated ${body.table} with ${res.affectedRows} rows`);
		result(null, { affectedRows: res.affectedRows });
	} catch (err) {
		console.log('error: ', err);
		result(err, null);
	} finally {
		if (conn) conn.release(); //release to pool
	}
};

Common.updateAdvance = async (body, result) => {
	let conn;
	let query = new StringBuilder('UPDATE ');

	try {
		var values = [],
			params = [];

		// FROM
		if (body.from) {
			query.append(`${body.from}`);
		}

		//INNER JOIN
		if (body.inner_join) {
			for (var key in body.inner_join) {
				query.appendLine(`INNER JOIN ${key} ON ${body.inner_join[key]}`);
			}
		}

		// LEFT JOIN
		if (body.left_join) {
			for (var key in body.left_join) {
				query.appendLine(`LEFT JOIN ${key} ON ${body.left_join[key]}`);
			}
		}

		// SET
		query.appendLine('SET ');
		if (body.set) {
			for (var key in body.set) {
				if (body.set[key] != '') {
					values.push(`${key} = ${body.set[key]}`);
				}
			}
			query.append(values.join(', '));
		}

		// WHERE
		if (body.where) {
			values = [];
			for (var key in body.where) {
				if (body.where[key].value) {
					if (body.where[key].operator === 'IN') {
						values.push(`${key} ${body.where[key].operator} (?)`);
						params.push(body.where[key].value.replace(/(^[,\s]+)|([,\s]+$)/g, ''));
					} else if (body.where[key].operator === 'LIKE') {
						values.push(`${key} ${body.where[key].operator} ?`);
						params.push(`%${body.where[key].value}%`);
					} else if (key.includes('FIND_IN_SET(')) {
						if (body.where[key].list) {
							values.push(`${key}, ?) ${body.where[key].operator} ?`);
							params.push(body.where[key].list);
							params.push(body.where[key].value);
						}
					} else {
						values.push(`${key} ${body.where[key].operator} ?`);
						params.push(body.where[key].value);
					}
				}
			}
			query.appendLine(`WHERE ${values.join(' AND ')}`);
		}

		conn = await sql.getConnection();
		const res = await conn.query(query.toString(), params);
		if (res.affectedRows == 0) {
			// not found data with the id
			result({ kind: 'not_found' }, null);
			return;
		}
		console.log(`updated ${body.table} with ${res.affectedRows} rows`);
		result(null, { affectedRows: res.affectedRows });
	} catch (err) {
		console.log('error: ', err);
		result(err, null);
	} finally {
		if (conn) conn.release(); //release to pool
	}
};

Common.remove = async (body, result) => {
	let conn;

	try {
		var where = [],
			values = [],
			data = body.data;

		for (var key in data) {
			where.push(`${key} = ?`);
			values.push(data[key]);
		}

		var query = `UPDATE ${body.table} SET status = 0, updateDate = NOW(), updateBy = ${body.userId
			} WHERE ${where.join(' AND ')}`;

		conn = await sql.getConnection();
		const res = await conn.query(query, values);
		if (res.affectedRows == 0) {
			// not found data with the id
			result({ kind: 'not_found' }, null);
			return;
		}
		console.log(`deleted ${body.table} with ${res.affectedRows} rows`);
		result(null, res);
	} catch (err) {
		console.log('error: ', err);
		result(err, null);
	} finally {
		if (conn) conn.release(); //release to pool
	}
};

Common.callProcedure = async (body, result) => {
	let conn;

	try {
		var param = [],
			values = [],
			data = body.data;

		for (var key in data) {
			param.push(`?`);
			values.push(data[key]);
		}

		var query = `CALL ${body.procedure} (${param.join(', ')})`;

		conn = await sql.getConnection();
		const res = await conn.query(query, values);
		// if (res.affectedRows == 0) {
		// 	// not found data with the id
		// 	result({ kind: 'not_found' }, null);
		// 	return;
		// }
		// console.log(`deleted ${body.table} with ${res.affectedRows} rows`);
		result(null, res[0]);
	} catch (err) {
		console.log('error: ', err);
		result(err, null);
	} finally {
		if (conn) conn.release(); //release to pool
	}
};

Common.imageToBase64 = (url) => {
	let buff = fs.readFileSync(url);
	return buff.toString('base64');
};

Common.now = () => {
	return moment.tz(config.TIMEZONE).format(config.DATETIME);
};

Common.dateToText = (value) => {
	return moment(value).tz(config.TIMEZONE).format(config.DATE_FORMAT);
};

Common.bitToValue = (value) => {
	if (value == null) {
		return 0;
	} else if (value.data) {
		return value.data[0];
	} else {
		return value ? 1 : 0;
	}
};

Common.trimChildren = (data) => {
	var y;
	for (var x in data) {
		y = data[x].children;
		if (
			y === 'null' ||
			y === null ||
			y === '' ||
			typeof y === 'undefined' ||
			(y instanceof Object && Object.keys(y).length == 0)
		) {
			delete data[x].children;
		}
		if (y instanceof Object) y = Common.trimChildren(y);
	}
	return data;
};

Common.nestChildren = (items, id = null, link = 'parentId') =>
	items
		.filter((item) => item[link] === id)
		.map((item) => ({ ...item, children: Common.nestChildren(items, item.id, link) }));

// Media
Common.streamingAudio = (req, res) => {
	var filePath = req.body.filePath;
	var stat = fs.statSync(filePath);

	console.log('playing', filePath);

	res.writeHead(200, {
		'Content-Type': 'audio/mpeg',
		'Content-Length': stat.size,
	});

	var readStream = fs.createReadStream(filePath);
	// We replaced all the event handlers with a simple call to util.pump()
	// util.pump(readStream, response);
	readStream.pipe(res);
};

Common.sendFile = (req, res) => {
	// res.sendFile(path.resolve(req.body.filePath));
	res.download(path.resolve(req.body.filePath), (err) => {
		if (err) {
			res.send({ error: { ...err, message: err.message } });
		}
	});
};

Common.replaceJson = (value) => {
	return value == null
		? null
		: value
			.replace(/\\\\"/g, '"')
			.replace(/\\"/g, '"')
			.replace(/"{/g, '{')
			.replace(/}"/g, '}')
			.replace(/"\[/g, '[')
			.replace(/]"/g, ']')
			.replace('"true"', 'true')
			.replace('"false"', 'false');
};

module.exports = Common;
