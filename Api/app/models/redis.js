const redis = require('redis');
const dbConfig = require('../config/db.config');

if (!client) {
	var client = redis.createClient(dbConfig.REDIS_PORT, dbConfig.REDIS_HOST);
}

client.on('error', function (err) {
	console.log('Redis Error: ', err);
	client = null;
});

client.on('connect', function () {
	console.log('Redis connected successfully.');
});

const Redis = {};

Redis.set = function (key, value, expire) {
	return new Promise(function (resolve, reject) {
		if (typeof value == 'undefined') {
			return reject(key + ' is empty');
		}
		client.set(dbConfig.REDIS_PREFIX + key, value, function (err, result) {
			if (err) {
				return reject(err);
			}
			if (!isNaN(expire) && expire > 0) {
				client.expire(key, parseInt(expire));
			}
			return resolve(result);
		});
	});
};

Redis.get = function (key) {
	return new Promise(function (resolve, reject) {
		client.get(dbConfig.REDIS_PREFIX + key, function (err, result) {
			if (err) {
				return reject(err);
			}
			return resolve(result);
		});
	});
};

Redis.del = function (key) {
	return new Promise(function (resolve, reject) {
		client.del(dbConfig.REDIS_PREFIX + key, function (err, result) {
			if (err) {
				return reject(err);
			}
			return resolve(result);
		});
	});
};

module.exports = Redis;
