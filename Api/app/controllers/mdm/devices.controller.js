const Common = require('../../models/common.model');
const { now } = require('../../models/common.model');
const tableName = 'mdm_devices';

// Find items with condition
exports.getDefault = (req, res) => {
	var json = {
		table: tableName,
		data: { id_customer: req.customerId, ...req.body },
	};

	Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};

// getBy DiaBan
exports.get = (req, res) => {
	var json = {
		select: {
			'A.*': '',
			'C.name': 'tenDiaBan',
			'D.name': 'tenConfig',
			'E.name': 'tenGroup',
		},
		from: `${tableName} A`,
		inner_join: {
			'portal_account_diaBan B': 'A.id_diaBan = B.id_diaBan AND B.status = 1',
			'mdm_diaBan C': 'A.id_diaBan = C.id',
			'mdm_configurations D': 'A.id_configuration = D.id',
			'mdm_groups E': 'A.id_group = E.id',
		},
		whereGroup: {
			status: [
				{
					'A.status': {
						operator: '=',
						value: 1,
					},
				},
			],
			id_customer: [
				{
					'A.id_customer': {
						operator: '=',
						value: req.customerId,
					},
				},
			],
			id_account: [
				{
					'B.id_account': {
						operator: '=',
						value: req.userId,
					},
				},
			],
		},
		orderBy: 'A.name',
	};
	Common.getAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};

// Create and Save a new item
exports.create = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body, id_customer: req.customerId, createBy: req.userId, createDate: Common.now() },
	};

	// Save data in the database
	Common.create(json, (err, data) => {
		if (err)
			res.send({
				error: { ...err, message: err.message } || `Some error occurred while creating the ${json.table}.`,
			});
		else res.send({ data: data, message: 'Item was created successfully!' });
	});
};

// Update a item identified by the condition in the request
exports.update = (req, res) => {
	// Validate Request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body.data, updateBy: req.userId, updateDate: Common.now() },
		condition: req.body.condition,
	};

	Common.update(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
				});
			}
		} else res.send({ data: data, message: 'Item was updated successfully!' });
	});
};

// Update địa bàn cho nhiều thiết bị
exports.updateDiaBan = (req, res) => {
	var json = {
		from: `mdm_devices A`,
		set: {
			'A.id_diaBan': req.body.id_diaBan,
			'A.updateBy': req.userId,
			'A.updateDate': 'now()',
		},
		where: {
			'A.id_customer': {
				operator: '=',
				value: req.customerId,
			},
			'A.status': {
				operator: '=',
				value: 1,
			},
			'FIND_IN_SET(A.id': {
				list: req.body.id_devices,
				operator: '>=',
				value: 1,
			},
		},
	};

	Common.updateAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.from} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Update data successfully!' });
	});
};

// Update cấu hình cho nhiều thiết bị
exports.updateConfiguration = (req, res) => {
	var json = {
		from: `mdm_devices A`,
		set: {
			'A.id_configuration_old': 'A.id_configuration',
			'A.id_configuration': req.body.id_configuration,
			'A.updateBy': req.userId,
			'A.updateDate': 'now()',
		},
		where: {
			'A.id_customer': {
				operator: '=',
				value: req.customerId,
			},
			'A.status': {
				operator: '=',
				value: 1,
			},
			'FIND_IN_SET(A.id': {
				list: req.body.id_devices,
				operator: '>=',
				value: 1,
			},
		},
	};

	Common.updateAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.from} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Update data successfully!' });
	});
};

// Update nhóm cho nhiều thiết bị
exports.updateGroup = (req, res) => {
	var json = {
		from: `mdm_devices A`,
		set: {
			'A.id_group': req.body.id_group,
			'A.updateBy': req.userId,
			'A.updateDate': 'now()',
		},
		where: {
			'A.id_customer': {
				operator: '=',
				value: req.customerId,
			},
			'A.status': {
				operator: '=',
				value: 1,
			},
			'FIND_IN_SET(A.id': {
				list: req.body.id_devices,
				operator: '>=',
				value: 1,
			},
		},
	};

	Common.updateAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.from} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Update data successfully!' });
	});
};

// Update các thông tin địa bàn, cấu hình, nhóm cho nhiều thiết bị
exports.updateAllSetting = (req, res) => {
	var json = {
		from: `mdm_devices A`,
		set: {
			'A.id_group': req.body.id_group,
			'A.id_diaBan': req.body.id_diaBan,
			'A.id_configuration_old': 'A.id_configuration',
			'A.id_configuration': req.body.id_configuration,
			'A.description': req.body.description,
			'A.updateBy': req.userId,
			'A.updateDate': 'now()',
		},
		where: {
			'A.id_customer': {
				operator: '=',
				value: req.customerId,
			},
			'A.status': {
				operator: '=',
				value: 1,
			},
			'FIND_IN_SET(A.id': {
				list: req.body.id_devices,
				operator: '>=',
				value: 1,
			},
		},
	};

	Common.updateAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.from} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Update data successfully!' });
	});
};

// Delete items with the specified data in body in the request
exports.delete = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
		userId: req.userId,
	};

	Common.remove(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ message: `Item was deleted successfully!` });
	});
};

// Kích hoạt thiết bị
exports.register = (req, res) => {
	// Validate Request
	if (!req.body.id) {
		res.send({
			error: { message: 'ID can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { id_customer: req.customerId, serialNumber: req.body.id },
	};

	Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({ error: { message: 'Device not found.' } });
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else {
			if (data[0].registedTime != null) res.send({ error: { message: 'This device has been used.' } });
			else {
				json = {
					table: tableName,
					data: { registedTime: Common.now() },
					condition: { id: data[0].id },
				};

				Common.update(json, (err, data) => {
					if (err) {
						if (err.kind === 'not_found') {
							res.send({
								error: { message: 'Not found data.' },
							});
						} else {
							res.send({
								error:
									{ ...err, message: err.message } ||
									`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
							});
						}
					} else res.send({ data: data, message: 'Device registed successfully!' });
				});
			}
		}
	});
};

exports.filesDownload = (req, res) => {
	
};
