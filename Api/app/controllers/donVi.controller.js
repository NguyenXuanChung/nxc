const Common = require('../models/common.model');

// Find items with condition
exports.get = (req, res) => {
	var json = {
		table: 'donvi',
		data: req.body,
	};

	Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: `Get ${json.table} was successfully!` });
	});
};

exports.getTree = (req, res) => {
	var json = {
		procedure: 'donvi_getTree',
		data: { id_customer: req.customerId, ...req.body },
	};

	Common.callProcedure(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
				});
			}
		} else {
			// console.log(nest(data, 0));
			res.send({ data: nest(data, 0), message: 'Get tree donvi successfully!' });
		}
	});
};

exports.getTree_Value = (req, res) => {
	var json = {
		procedure: 'donvi_getTree',
		data: { id_customer: req.customerId, ...req.body },
	};

	Common.callProcedure(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
				});
			}
		} else {
			res.send({ data: nestValue(data, 0), message: 'Get tree donvi successfully!' });
		}
	});
};

exports.getTreeNode = (req, res) => {
	var json = {};
	if (!req.body) {
		json = {
			procedure: 'donvi_getTree',
			data: { id_customer: req.customerId, ...req.body },
		};
	} else {
		json = {
			procedure: 'donvi_getTree',
			data: { id_customer: req.customerId, id_trungTam: 1, tuNgay: Common.now(), denNgay: Common.now() },
		};
	}

	Common.callProcedure(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
				});
			}
		} else {
			// console.log(nest(data, 0));
			res.send({ data: Common.trimChildren(nestTreeNode(data, 0)), message: 'Get tree donvi successfully!' });
		}
	});
};

const nest = (items, donvi_id = null, link = 'id_capTren') =>
	items.filter((item) => item[link] === donvi_id).map((item) => ({ ...item, children: nest(items, item.donvi_id) }));

const nestValue = (items, donvi_id = null, link = 'id_capTren') =>
	items
		.filter((item) => item[link] === donvi_id)
		.map((item) => ({ data: item.donvi_id, label: item.tenDonVi, children: nestValue(items, item.donvi_id) }));

const nestTreeNode = (items, donvi_id = null, link = 'id_capTren') =>
	items
		.filter((item) => item[link] === donvi_id)
		.map((item) => ({
			data: { ...item },
			expanded: item.id_capTren == 0 ? true : false,
			children: nestTreeNode(items, item.donvi_id),
		}));
