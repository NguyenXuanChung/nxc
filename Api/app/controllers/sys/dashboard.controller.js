const Common = require('../../models/common.model');
const bcrypt = require('bcryptjs');

// Find items with condition
exports.get = async (req, res) => {
	// DemSL ThietBi
	var json1 = {
		select: {
			'COUNT(id)': 'sLDevice',
		},
		from: `mdm_devices`,
		where: {
			'id_customer': {
				operator: '=',
				value: req.customerId,
			},
			'status': {
				operator: '=',
				value: 1,
			},
		},
	};
	var data1;
	await Common.getAdvance(json1, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else data1 = data[0].sLDevice;
	});

		// DemSL DiaBan
	var json2 = {
		select: {
			'COUNT(*)': 'slDiaBan',
		},
		from: `mdm_diaban`,
		where: {
			'id_customer': {
				operator: '=',
				value: req.customerId,
			},
			'status': {
				operator: '=',
				value: 1,
			},
		},
	};
	var data2;
	await Common.getAdvance(json2, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				// res.send({
				// 	// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
				// 	error: { message: 'Not found data.' },
				// });
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else data2 = data[0].slDiaBan;
	});

	// Lay ra Thong bao dang phat hoac chuan bi phat. 
	var json3 = {
		select: {
			'*': '',
		},
		from: `mdm_thongbao`,
		where: {
			'id_customer': {
				operator: '=',
				value: req.customerId,
			},
			'status': {
				operator: '=',
				value: 1,
			},
			'denNgay': {
				operator: '>',
				value: Common.now(),
			},
			orderBy: 'noiDung'
		},
	};
	var data3;
	await Common.getAdvance(json3, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else data3 = data;
	});


	// lay ra sl thiet bi theo dia ban.
	var json = {
		select: {
			'COUNT(B.id)': 'soThietBi',
			'A.name': 'name',
			'A.id': 'id',
		},
		from: `mdm_diaban A`,
		left_join: {
			'mdm_devices B': `B.id_diaBan = A.id AND B.status = 1`,
		},
		inner_join:{
			'portal_account_diaBan C': 'A.id = C.id_diaBan AND C.status = 1',
		},
		where: {
			'A.id_customer': {
				operator: '=',
				value: req.customerId,
			},
			'A.status': {
				operator: '=',
				value: 1,
			},
			'C.id_account': {
				operator: '=',
				value: req.userId,
			},
		},
		groupBy: 'A.name, A.id',
	};
	var data4;
	await Common.getAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else data4 = data;
	});
	await res.send({ data: {sLDevice: data1, sLDiaBan: data2, thongBaos: data3, diaBanDevice: data4}, message: 'Retrieve data successfully!' });
};

