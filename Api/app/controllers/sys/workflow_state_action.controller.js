const Common = require('../../models/common.model');
const bcrypt = require('bcryptjs');
const tableName = 'sys_workflow_state_action';

// Find items with condition
exports.get = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
	};

	Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};

// Create and Save a new item
exports.create = (req, res) => {
	// Validate request
	if (!req.body || req.body.length == 0) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body, createBy: req.userId, createDate: Common.now() },
	};

	// Save data in the database
	Common.create(json, (err, data) => {
		if (err)
			res.send({
				error: { ...err, message: err.message } || `Some error occurred while creating the ${json.table}.`,
			});
		else res.send({ data: data, message: 'Item was created successfully!' });
	});
};

// Update a item identified by the condition in the request
exports.update = (req, res) => {
	// Validate Request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body.data, updateBy: req.userId, updateDate: Common.now() },
		condition: req.body.condition,
	};

	Common.update(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
				});
			}
		} else res.send({ data: data, message: 'Item was updated successfully!' });
	});
};

// Delete items with the specified data in body in the request
exports.delete = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
		userId: req.userId,
	};

	Common.remove(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else {
			// Xóa các dữ liệu của bảng con
			json = {
				table: 'scyk_dm_phanloaisuco',
				data: { status: 0, updateBy: req.userId, updateDate: Common.now() },
				condition: { id_dm_capDoNguyCo: req.body.id, status: 1 },
			};

			Common.update(json, (err, data) => {
				if (err) {
					if (err.kind === 'not_found') {
						res.send({
							error: { message: 'Not found data.' },
						});
					} else {
						res.send({
							error:
								{ ...err, message: err.message } ||
								`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
						});
					}
				} else res.send({ data: data, message: 'Item was updated successfully!' });
			});
		}
	});
};
