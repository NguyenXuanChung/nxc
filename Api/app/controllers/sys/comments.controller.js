const Common = require('../../models/common.model');
const bcrypt = require('bcryptjs');
const _ = require('lodash');
const tableName = 'sys_comments';

// Find items with condition
exports.get = (req, res) => {
	var json = {
		table: tableName,
		data: { id_customer: req.customerId, ...req.body },
	};

	Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};

exports.getByID_Data = (req, res) => {
	var json = {
		select: {
			"IFNULL(B.fullname, CONCAT_WS(' ', C.hoDem, C.ten))": 'nguoiTao',
			'B.id_canbo': '',
			'A.*': '',
		},
		from: `${tableName} A`,
		left_join: {
			'portal_account B': `A.createBy = B.portal_account_id`,
			'canbo C': `B.id_canbo = C.canbo_id`,
		},
		where: {
			'A.status': {
				operator: '=',
				value: 1,
			},
			'A.id_customer': {
				operator: '=',
				value: req.customerId,
			},
			'A.id_dataTypes': {
				operator: '=',
				value: req.body.id_dataTypes || -1,
			},
			'A.id_data': {
				operator: '=',
				value: req.body.id_data || -1,
			},
			'IFNULL(A.viewableRoleLevel, 0)': {
				operator: '<=',
				value: req.body.roleLevel,
			},
		},
		orderBy: 'A.createDate',
	};

	Common.getAdvance(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					// error: `Not found ${json.table} with ${JSON.stringify(json.data)}.`,
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};

// Create and Save a new item
exports.create = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body, id_customer: req.customerId, createBy: req.userId, createDate: Common.now() },
	};

	// Save data in the database
	Common.create(json, (err, data) => {
		if (err)
			res.send({
				error: { ...err, message: err.message } || `Some error occurred while creating the ${json.table}.`,
			});
		else res.send({ data: data, message: 'Item was created successfully!' });
	});
};

// Update a item identified by the condition in the request
exports.update = (req, res) => {
	// Validate Request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body.data, updateBy: req.userId, updateDate: Common.now() },
		condition: req.body.condition,
	};

	Common.update(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
				});
			}
		} else res.send({ data: data, message: 'Item was updated successfully!' });
	});
};

// Delete items with the specified data in body in the request
exports.delete = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
		userId: req.userId,
	};

	Common.remove(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ message: `Item was deleted successfully!` });
	});
};
