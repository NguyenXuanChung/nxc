const Common = require('../../models/common.model');
const bcrypt = require('bcryptjs');
const tableName = 'portal_account';

const Account = function () {};

// Create and Save a new item
exports.create = (req, res) => {
	// Validate request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: { ...req.body, id_customer: req.customerId },
	};

	// Save data in the database
	Common.create(json, (err, data) => {
		if (err)
			res.send({
				error: { ...err, message: err.message } || `Some error occurred while creating the ${json.table}.`,
			});
		else res.send({ data: data, message: 'Account created was successfully!' });
	});
};

exports.add = (req, res) => {
	req.body.password = bcrypt.hashSync(req.body.password, 8);
	var json = {
		procedure: 'portal_account_add',
		data: { ...req.body, id_customer: req.customerId, createBy: req.userId }, // id_canBo, username, password, id_donVi, id_portal_nhomQuyen
	};

	Common.callProcedure(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({ error: { message: 'Not found data.' } });
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
				});
			}
		} else {
			if (data[0].sqlError == '') {
				res.send({ data: data, message: 'Account added successfully!' });
			} else {
				res.send({ error: { message: data[0].sqlError } });
			}
		}
	});
};

exports.edit = (req, res) => {
	if (req.body.password != '') {
		req.body.password = bcrypt.hashSync(req.body.password, 8);
	}

	var json = {
		procedure: 'portal_account_edit',
		data: { ...req.body, updateBy: req.userId }, // id_canBo, username, password, id_donVi, id_portal_nhomQuyen
	};

	Common.callProcedure(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({ error: { message: 'Not found data.' } });
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
				});
			}
		} else {
			if (data[0].sqlError == '') {
				res.send({ data: data, message: 'Account edited successfully!' });
			} else {
				res.send({ error: { message: data[0].sqlError } });
			}
		}
	});
};

// Find items with condition
exports.get = (req, res) => {
	var json = {
		table: tableName,
		data: { ...req.body, id_customer: req.customerId },
	};

	Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Retrieve data successfully!' });
	});
};

exports.search = (req, res) => {
	var json = {
		procedure: 'portal_account_search',
		data: { ...req.body, id_customer: req.customerId }, // username, maCanBo, hoTen
	};

	Common.callProcedure(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Your password was changed successfully!' });
	});
};

exports.getCanBoChuaCoTaiKhoan = (req, res) => {
	var json = {
		procedure: 'portal_account_getCanBoChuaCoTaiKhoan',
		data: { id_customer: req.customerId, id_donVi: req.body.id_donVi },
	};

	Common.callProcedure(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: data, message: 'Data loaded successfully!' });
	});
};

exports.getMenu = (req, res) => {
	var json = {
		procedure: 'portal_account_getMenu',
		data: { _id_account: req.userId },
	};

	Common.callProcedure(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ data: Common.trimChildren(nest(data, 0)), message: 'Data loaded successfully!' });
	});
};
const nest = (items, id = null, link = 'parent_id') =>
	items
		.filter((item) => item[link] === id)
		.map((item) => ({
			name: item.name,
			url: item.url,
			icon: item.icon,
			// attributes: { collapse: true },
			children: nest(items, item.id),
		}));

// Update a item identified by the condition in the request
exports.update = (req, res) => {
	// Validate Request
	if (!req.body) {
		res.send({
			error: { message: 'Content can not be empty!' },
		});
	}

	var json = {
		table: tableName,
		data: req.body.data,
		condition: req.body.condition,
	};

	Common.update(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error updating ${json.table} with ${JSON.stringify(json.condition)}.`,
				});
			}
		} else res.send({ data: data, message: 'Account was updated successfully!' });
	});
};

Account.changePassword = async (req, res) => {
	var json = {
		table: 'portal_account_password',
		data: { id_portal_account: req.userId, status: 1 },
	};

	let account_password = [];

	await Common.get(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Error retrieving ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else account_password = data[0];
	});

	const comparePass = await bcrypt.compare(req.body.password, account_password.password);
	if (comparePass) {
		const encryptPassword = bcrypt.hashSync(req.body.newPassword, 8);
		json = {
			procedure: 'portal_account_changePassword',
			data: { id_portal_account: req.userId, password: encryptPassword, createBy: req.userId },
		};

		await Common.callProcedure(json, (err, data) => {
			if (err) {
				if (err.kind === 'not_found') {
					res.send({
						error: { message: 'Not found data.' },
					});
				} else {
					res.send({
						error:
							{ ...err, message: err.message } ||
							`Error retrieving ${json.procedure} with ${JSON.stringify(json.data)}`,
					});
				}
			} else res.send({ data: data, message: 'Your password was changed successfully!' });
		});
	} else {
		return res.send({ error: { message: 'Incorrect password' } });
	}
};
exports.changePassword = Account.changePassword;

// Delete items with the specified data in body in the request
exports.delete = (req, res) => {
	var json = {
		table: tableName,
		data: req.body,
		userId: req.userId,
	};

	Common.remove(json, (err, data) => {
		if (err) {
			if (err.kind === 'not_found') {
				res.send({
					error: { message: 'Not found data.' },
				});
			} else {
				res.send({
					error:
						{ ...err, message: err.message } ||
						`Could not delete ${json.table} with ${JSON.stringify(json.data)}`,
				});
			}
		} else res.send({ message: { message: 'Item was deleted successfully!' } });
	});
};
