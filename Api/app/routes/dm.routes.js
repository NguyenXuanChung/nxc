module.exports = (app) => {
	const VerifyToken = require('../controllers/VerifyToken');
	const tinhs = require('../controllers/dm/dm_tinh.controller');
	const huyens = require('../controllers/dm/dm_huyen.controller');
	const xa = require('../controllers/dm/dm_xa.controller');

	//#region dm_tinh
	// Retrieve data with condition in body
	app.post('/dm/dm-tinh/get', VerifyToken, tinhs.get);

	// Create a new item
	app.post('/dm/dm-tinh/insert', VerifyToken, tinhs.create);

	// Update a item with id
	app.post('/dm/dm-tinh/update', VerifyToken, tinhs.update);

	// Delete a item with id
	app.post('/dm/dm-tinh/delete', VerifyToken, tinhs.delete);
	//#endregion

	//#region dm_huyen
	// Retrieve data with condition in body
	app.post('/dm/dm-huyen/get', VerifyToken, huyens.get);

	// Create a new item
	app.post('/dm/dm-huyen/insert', VerifyToken, huyens.create);

	// Update a item with id
	app.post('/dm/dm-huyen/update', VerifyToken, huyens.update);

	// Delete a item with id
	app.post('/dm/dm-huyen/delete', VerifyToken, tinhs.delete);
	//#endregion

	//#region dm_xa
	// Retrieve data with condition in body
	app.post('/dm/dm-xa/get', VerifyToken, xa.get);

	// Create a new item
	app.post('/dm/dm-xa/insert', VerifyToken, xa.create);

	// Update a item with id
	app.post('/dm/dm-xa/update', VerifyToken, xa.update);

	// Delete a item with id
	app.post('/dm/dm-xa/delete', VerifyToken, xa.delete);
	//#endregion
};
