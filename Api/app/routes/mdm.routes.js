module.exports = (app) => {
	const VerifyToken = require('../controllers/VerifyToken');
	const apps = require('../controllers/mdm/applications.controller');
	const appVersions = require('../controllers/mdm/applicationVersions.controller');
	const confAppParameters = require('../controllers/mdm/configurationApplicationParameters.controller');
	const confApps = require('../controllers/mdm/configurationApplications.controller');
	const confAppSettings = require('../controllers/mdm/configurationApplicationSettings.controller');
	const confFiles = require('../controllers/mdm/configurationFiles.controller');
	const configurations = require('../controllers/mdm/configurations.controller');
	const devAppSettings = require('../controllers/mdm/deviceApplicationSettings.controller');
	const devices = require('../controllers/mdm/devices.controller');
	const devStatuses = require('../controllers/mdm/deviceStatuses.controller');
	const diaBans = require('../controllers/mdm/diaBan.controller');
	const groups = require('../controllers/mdm/groups.controller');
	const icons = require('../controllers/mdm/icons.controller');
	const pendPushs = require('../controllers/mdm/pendingPushes.controller');
	const pluginAuditLogs = require('../controllers/mdm/pluginAuditLog.controller');
	const pluginDevlLogs = require('../controllers/mdm/pluginDevicelogLog.controller');
	const pluginDevlSetRuleDevs = require('../controllers/mdm/pluginDevicelogSettingRuleDevices.controller');
	const pluginDevlSettings = require('../controllers/mdm/pluginDevicelogSettings.controller');
	const pluginDevlSetRules = require('../controllers/mdm/pluginDevicelogSettingsRules.controller');
	const pluginMessMess = require('../controllers/mdm/pluginMessagingMessages.controller');
	const pushMessages = require('../controllers/mdm/pushMessages.controller');
	const settings = require('../controllers/mdm/settings.controller');
	const upFiles = require('../controllers/mdm/uploadedFiles.controller');
	const userDevGroupAccess = require('../controllers/mdm/userDeviceGroupsAccess.controller');

	//#region applications
	// Retrieve data with condition in body
	app.post('/mdm/applications/get', VerifyToken, apps.get);

	app.post('/mdm/applications/checkpackage', VerifyToken, apps.checkApkPackage);

	// Create a new item
	app.post('/mdm/applications/insert', VerifyToken, apps.create);

	// Update a item with id
	app.post('/mdm/applications/update', VerifyToken, apps.update);

	// Delete a item with id
	app.post('/mdm/applications/delete', VerifyToken, apps.delete);
	//#endregion

	//#region application_versions
	// Retrieve data with condition in body
	app.post('/mdm/application-versions/get', VerifyToken, appVersions.get);

	// Create a new item
	app.post('/mdm/application-versions/insert', VerifyToken, appVersions.create);

	// Update a item with id
	app.post('/mdm/application-versions/update', VerifyToken, appVersions.update);

	// Delete a item with id
	app.post('/mdm/application-versions/delete', VerifyToken, appVersions.delete);
	//#endregion

	//#region configuration_application_parameters
	// Retrieve data with condition in body
	app.post('/mdm/configuration-application-parameters/get', VerifyToken, confAppParameters.get);

	// Create a new item
	app.post('/mdm/configuration-application-parameters/insert', VerifyToken, confAppParameters.create);

	// Update a item with id
	app.post('/mdm/configuration-application-parameters/update', VerifyToken, confAppParameters.update);

	// Delete a item with id
	app.post('/mdm/configuration-application-parameters/delete', VerifyToken, confAppParameters.delete);
	//#endregion

	//#region configuration-applications
	// Retrieve data with condition in body
	app.post('/mdm/configuration-applications/get', VerifyToken, confApps.get);

	// Create a new item
	app.post('/mdm/configuration-applications/insert', VerifyToken, confApps.create);

	// Update a item with id
	app.post('/mdm/configuration-applications/update', VerifyToken, confApps.update);

	// Delete a item with id
	app.post('/mdm/configuration-applications/delete', VerifyToken, confApps.delete);
	//#endregion

	//#region configuration-application-settings
	// Retrieve data with condition in body
	app.post('/mdm/configuration-application-settings/get', VerifyToken, confAppSettings.get);

	// Create a new item
	app.post('/mdm/configuration-application-settings/insert', VerifyToken, confAppSettings.create);

	// Update a item with id
	app.post('/mdm/configuration-application-settings/update', VerifyToken, confAppSettings.update);

	// Delete a item with id
	app.post('/mdm/configuration-application-settings/delete', VerifyToken, confAppSettings.delete);
	//#endregion

	//#region configuration-files
	// Retrieve data with condition in body
	app.post('/mdm/configuration-files/get', VerifyToken, confFiles.get);

	// Create a new item
	app.post('/mdm/configuration-files/insert', VerifyToken, confFiles.create);

	// Update a item with id
	app.post('/mdm/configuration-files/update', VerifyToken, confFiles.update);

	// Delete a item with id
	app.post('/mdm/configuration-files/delete', VerifyToken, confFiles.delete);
	//#endregion

	//#region configurations
	// Retrieve data with condition in body
	app.post('/mdm/configurations/get', VerifyToken, configurations.get);

	// Create a new item
	app.post('/mdm/configurations/insert', VerifyToken, configurations.create);

	// Update a item with id
	app.post('/mdm/configurations/update', VerifyToken, configurations.update);

	// Delete a item with id
	app.post('/mdm/configurations/delete', VerifyToken, configurations.delete);
	//#endregion

	//#region device-application-settings
	// Retrieve data with condition in body
	app.post('/mdm/device-application-settings/get', VerifyToken, devAppSettings.get);

	// Create a new item
	app.post('/mdm/device-application-settings/insert', VerifyToken, devAppSettings.create);

	// Update a item with id
	app.post('/mdm/device-application-settings/update', VerifyToken, devAppSettings.update);

	// Delete a item with id
	app.post('/mdm/device-application-settings/delete', VerifyToken, devAppSettings.delete);
	//#endregion

	//#region devices
	// Retrieve data with condition in body
	app.post('/mdm/devices/get', VerifyToken, devices.get);

	// Create a new item
	app.post('/mdm/devices/insert', VerifyToken, devices.create);

	// Update a item with id
	app.post('/mdm/devices/update', VerifyToken, devices.update);

	app.post('/mdm/devices/update-diaban', VerifyToken, devices.updateDiaBan);

	app.post('/mdm/devices/update-configuration', VerifyToken, devices.updateConfiguration);

	app.post('/mdm/devices/update-group', VerifyToken, devices.updateGroup);

	app.post('/mdm/devices/update-groupDiaBanConfigration', VerifyToken, devices.updateAllSetting);

	app.post('/mdm/devices/register', devices.register)

	// Delete a item with id
	app.post('/mdm/devices/delete', VerifyToken, devices.delete);
	//#endregion

	//#region device-statuses
	// Retrieve data with condition in body
	app.post('/mdm/device-statuses/get', VerifyToken, devStatuses.get);

	// Create a new item
	app.post('/mdm/device-statuses/insert', VerifyToken, devStatuses.create);

	// Update a item with id
	app.post('/mdm/device-statuses/update', VerifyToken, devStatuses.update);

	// Delete a item with id
	app.post('/mdm/device-statuses/delete', VerifyToken, devStatuses.delete);
	//#endregion

	//#region Địa bàn
	// Retrieve data with condition in body
	app.post('/mdm/diaban/get', VerifyToken, diaBans.get);
	app.post('/mdm/diaban/get-list', VerifyToken, diaBans.getList);
	app.post('/mdm/diaban/get-by-account', VerifyToken, diaBans.getByAccount);
	
	// Create a new item
	app.post('/mdm/diaban/insert', VerifyToken, diaBans.create);

	// Update a item with id
	app.post('/mdm/diaban/update', VerifyToken, diaBans.update);

	// Delete a item with id
	app.post('/mdm/diaban/delete', VerifyToken, diaBans.delete);
	//#endregion

	//#region groups
	// Retrieve data with condition in body
	app.post('/mdm/groups/get', VerifyToken, groups.get);

	// Create a new item
	app.post('/mdm/groups/insert', VerifyToken, groups.create);

	// Update a item with id
	app.post('/mdm/groups/update', VerifyToken, groups.update);

	// Delete a item with id
	app.post('/mdm/groups/delete', VerifyToken, groups.delete);
	//#endregion

	//#region icons
	// Retrieve data with condition in body
	app.post('/mdm/icons/get', VerifyToken, icons.get);

	// Create a new item
	app.post('/mdm/icons/insert', VerifyToken, icons.create);

	// Update a item with id
	app.post('/mdm/icons/update', VerifyToken, icons.update);

	// Delete a item with id
	app.post('/mdm/icons/delete', VerifyToken, icons.delete);
	//#endregion

	//#region pending-pushes
	// Retrieve data with condition in body
	app.post('/mdm/pending-pushes/get', VerifyToken, pendPushs.get);

	// Create a new item
	app.post('/mdm/pending-pushes/insert', VerifyToken, pendPushs.create);

	// Update a item with id
	app.post('/mdm/pending-pushes/update', VerifyToken, pendPushs.update);

	// Delete a item with id
	app.post('/mdm/pending-pushes/delete', VerifyToken, pendPushs.delete);
	//#endregion

	//#region plugin-audit-log
	// Retrieve data with condition in body
	app.post('/mdm/plugin-audit-log/get', VerifyToken, pluginAuditLogs.get);

	// Create a new item
	app.post('/mdm/plugin-audit-log/insert', VerifyToken, pluginAuditLogs.create);

	// Update a item with id
	app.post('/mdm/plugin-audit-log/update', VerifyToken, pluginAuditLogs.update);

	// Delete a item with id
	app.post('/mdm/plugin-audit-log/delete', VerifyToken, pluginAuditLogs.delete);
	//#endregion

	//#region plugin-devicelog-log
	// Retrieve data with condition in body
	app.post('/mdm/plugin-devicelog-log/get', VerifyToken, pluginDevlLogs.get);

	// Create a new item
	app.post('/mdm/plugin-devicelog-log/insert', VerifyToken, pluginDevlLogs.create);

	// Update a item with id
	app.post('/mdm/plugin-devicelog-log/update', VerifyToken, pluginDevlLogs.update);

	// Delete a item with id
	app.post('/mdm/plugin-devicelog-log/delete', VerifyToken, pluginDevlLogs.delete);
	//#endregion

	//#region plugin-devicelog-setting-rule-devices
	// Retrieve data with condition in body
	app.post('/mdm/plugin-devicelog-setting-rule-devices/get', VerifyToken, pluginDevlSetRuleDevs.get);

	// Create a new item
	app.post('/mdm/plugin-devicelog-setting-rule-devices/insert', VerifyToken, pluginDevlSetRuleDevs.create);

	// Update a item with id
	app.post('/mdm/plugin-devicelog-setting-rule-devices/update', VerifyToken, pluginDevlSetRuleDevs.update);

	// Delete a item with id
	app.post('/mdm/plugin-devicelog-setting-rule-devices/delete', VerifyToken, pluginDevlSetRuleDevs.delete);
	//#endregion

	//#region plugin-devicelog-settings
	// Retrieve data with condition in body
	app.post('/mdm/plugin-devicelog-settings/get', VerifyToken, pluginDevlSettings.get);

	// Create a new item
	app.post('/mdm/plugin-devicelog-settings/insert', VerifyToken, pluginDevlSettings.create);

	// Update a item with id
	app.post('/mdm/plugin-devicelog-settings/update', VerifyToken, pluginDevlSettings.update);

	// Delete a item with id
	app.post('/mdm/plugin-devicelog-settings/delete', VerifyToken, pluginDevlSettings.delete);
	//#endregion

	//#region plugin-devicelog-settings-rules
	// Retrieve data with condition in body
	app.post('/mdm/plugin-devicelog-settings-rules/get', VerifyToken, pluginDevlSetRules.get);

	// Create a new item
	app.post('/mdm/plugin-devicelog-settings-rules/insert', VerifyToken, pluginDevlSetRules.create);

	// Update a item with id
	app.post('/mdm/plugin-devicelog-settings-rules/update', VerifyToken, pluginDevlSetRules.update);

	// Delete a item with id
	app.post('/mdm/plugin-devicelog-settings-rules/delete', VerifyToken, pluginDevlSetRules.delete);
	//#endregion

	//#region plugin-messaging-messages
	// Retrieve data with condition in body
	app.post('/mdm/plugin-messaging-messages/get', VerifyToken, pluginMessMess.get);

	// Create a new item
	app.post('/mdm/plugin-messaging-messages/insert', VerifyToken, pluginMessMess.create);

	// Update a item with id
	app.post('/mdm/plugin-messaging-messages/update', VerifyToken, pluginMessMess.update);

	// Delete a item with id
	app.post('/mdm/plugin-messaging-messages/delete', VerifyToken, pluginMessMess.delete);
	//#endregion

	//#region push-messages
	// Retrieve data with condition in body
	app.post('/mdm/push-messages/get', VerifyToken, pushMessages.get);

	// Create a new item
	app.post('/mdm/push-messages/insert', VerifyToken, pushMessages.create);

	// Update a item with id
	app.post('/mdm/push-messages/update', VerifyToken, pushMessages.update);

	// Delete a item with id
	app.post('/mdm/push-messages/delete', VerifyToken, pushMessages.delete);
	//#endregion

	//#region settings
	// Retrieve data with condition in body
	app.post('/mdm/settings/get', VerifyToken, settings.get);

	// Create a new item
	app.post('/mdm/settings/insert', VerifyToken, settings.create);

	// Update a item with id
	app.post('/mdm/settings/update', VerifyToken, settings.update);

	// Delete a item with id
	app.post('/mdm/settings/delete', VerifyToken, settings.delete);
	//#endregion

	//#region uploaded-files
	// Retrieve data with condition in body
	app.post('/mdm/uploaded-files/get', VerifyToken, upFiles.get);

	// Create a new item
	app.post('/mdm/uploaded-files/insert', VerifyToken, upFiles.create);

	// Update a item with id
	app.post('/mdm/uploaded-files/update', VerifyToken, upFiles.update);

	// Delete a item with id
	app.post('/mdm/uploaded-files/delete', VerifyToken, upFiles.delete);
	//#endregion

	//#region user-device-groups-access
	// Retrieve data with condition in body
	app.post('/mdm/user-device-groups-access/get', VerifyToken, userDevGroupAccess.get);

	// Create a new item
	app.post('/mdm/user-device-groups-access/insert', VerifyToken, userDevGroupAccess.create);

	// Update a item with id
	app.post('/mdm/user-device-groups-access/update', VerifyToken, userDevGroupAccess.update);

	// Delete a item with id
	app.post('/mdm/user-device-groups-access/delete', VerifyToken, userDevGroupAccess.delete);
	//#endregion
};
