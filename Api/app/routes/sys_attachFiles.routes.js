module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const attachs = require('../controllers/sys/attachFiles.controller');
  const details = require('../controllers/sys/attachFilesDetail.controller');
  
  //#region Attach file master
	// Retrieve data with condition in body
	app.post('/sys/attachfiles/get', VerifyToken, attachs.get);

	// Create a new item
	app.post('/sys/attachfiles/insert', VerifyToken, attachs.create);

	app.post('/sys/attachfiles/insert-multi', VerifyToken, attachs.insert);

	// Update a item with id
	app.post('/sys/attachfiles/update', VerifyToken, attachs.update);

	// Delete a item with id
	app.post('/sys/attachfiles/delete', VerifyToken, attachs.delete);
	
	// Upload files
	app.post('/sys/attachfiles/uploads', VerifyToken, attachs.uploads);

	// Download files
	app.post('/sys/attachfiles/download', VerifyToken, attachs.download);
  //#endregion

  //#region Attach file detail
	// Retrieve data with condition in body
	app.post('/sys/attachfiles-detail/get', VerifyToken, details.get);

	app.post('/sys/attachfiles-detail/getbytablename', VerifyToken, details.getByTableName);

	// Create a new item
	app.post('/sys/attachfiles-detail/insert', VerifyToken, details.create);

	// Update a item with id
	app.post('/sys/attachfiles-detail/update', VerifyToken, details.update);

	// Delete a item with id
  app.post('/sys/attachfiles-detail/delete', VerifyToken, details.delete);
  //#endregion
};