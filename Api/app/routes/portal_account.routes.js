module.exports = app => {
  const accounts = require("../controllers/sys/portal_account.controller");
  const VerifyToken = require("../controllers/VerifyToken");

  // Create a new portal_account
  app.post("/portal-accounts/insert", VerifyToken, accounts.create);

  app.post("/portal-accounts/add", VerifyToken, accounts.add);

  app.post("/portal-accounts/edit", VerifyToken, accounts.edit);

  // Retrieve a portal_accounts with condition in body
  app.post("/portal-accounts/get", VerifyToken, accounts.get);

  app.post("/portal-accounts/search", VerifyToken, accounts.search);

  app.post("/portal-accounts/getCanBoChuaCoTaiKhoan", VerifyToken, accounts.getCanBoChuaCoTaiKhoan);

  app.post("/portal-accounts/get-menu", VerifyToken, accounts.getMenu);

  // Update a portal_account with portal_account_id
  app.post("/portal-accounts/update", VerifyToken, accounts.update);

  // Change password
  app.post("/portal-accounts/changePassword", VerifyToken, accounts.changePassword);

  // Delete a portal_account with portal_account_id
  app.post("/portal-accounts/delete", VerifyToken, accounts.delete);
};
