module.exports = app => {
  const VerifyToken = require('../controllers/VerifyToken');
  const roleLevels = require('../controllers/sys/roleLevel.controller');
  const permissions = require('../controllers/sys/permission.controller');
  const roles = require('../controllers/sys/role.controller');
  const rolePermissions = require('../controllers/sys/role_permission.controller');
  const accountRoles = require('../controllers/sys/account_role.controller');
  const parameters = require('../controllers/sys/parameter.controller');
  const dashboards = require('../controllers/sys/dashboard.controller');
  
  //#region Role Level
	// Retrieve data with condition in body
  app.post('/sys/role-level/get', VerifyToken, roleLevels.get);
  //#endregion
  //#region  dashboard
  app.post('/sys/dashboard/get', VerifyToken, dashboards.get);

  //#endregion
  //#region Permission
  // Retrieve data with condition in body
  app.post('/sys/permission/get', VerifyToken, permissions.get);
  
  app.post('/sys/permission/gettree', VerifyToken, permissions.getTree);

  app.post('/sys/permission/get-all', VerifyToken, permissions.getAll);

  app.post('/sys/permission/gettree-value', VerifyToken, permissions.getTree_Value);

	// Create a new item
	app.post('/sys/permission/insert', VerifyToken, permissions.create);

	// Update a item with id
	app.post('/sys/permission/update', VerifyToken, permissions.update);

	// Delete a item with id
  app.post('/sys/permission/delete', VerifyToken, permissions.delete);
  //#endregion

  //#region Role
  // Retrieve data with condition in body
  app.post('/sys/role/get', VerifyToken, roles.get);
  
	// Create a new item
	app.post('/sys/role/insert', VerifyToken, roles.create);

	// Update a item with id
	app.post('/sys/role/update', VerifyToken, roles.update);

	// Delete a item with id
  app.post('/sys/role/delete', VerifyToken, roles.delete);
  //#endregion

  //#region Role_Permission
  // Retrieve data with condition in body
  app.post('/sys/role-permission/get', VerifyToken, rolePermissions.get);

  app.post('/sys/role-permission/getbyrole', VerifyToken, rolePermissions.getAllDmByID_Role);

  app.post('/sys/role-permission/gettablebyrole', VerifyToken, rolePermissions.getTableByID_Role);
  
	// Create a new item
	app.post('/sys/role-permission/insert', VerifyToken, rolePermissions.create);

	// Update a item with id
  app.post('/sys/role-permission/update', VerifyToken, rolePermissions.update);

  app.post('/sys/role-permission/update-role-permission', VerifyToken, rolePermissions.updateRolePermission);

	// Delete a item with id
  app.post('/sys/role-permission/delete', VerifyToken, rolePermissions.delete);
  //#endregion

  //#region portal_account_role
  // Retrieve data with condition in body
  app.post('/sys/account-role/get', VerifyToken, accountRoles.get);

  app.post('/sys/account-role/getallrolebyaccount', VerifyToken, accountRoles.getAllRoleByAccount);
  
	// Create a new item
	app.post('/sys/account-role/insert', VerifyToken, accountRoles.create);

	// Update a item with id
  app.post('/sys/account-role/update', VerifyToken, accountRoles.update);
  
  app.post('/sys/account-role/update-list', VerifyToken, accountRoles.updateBatch);

	// Delete a item with id
  app.post('/sys/account-role/delete', VerifyToken, accountRoles.delete);
  //#endregion

  //#region Parameter
  // Retrieve data with condition in body
  app.post('/sys/parameter/get', VerifyToken, parameters.get);
  
	// Create a new item
	app.post('/sys/parameter/insert', VerifyToken, parameters.create);

	// Update a item with id
	app.post('/sys/parameter/update', VerifyToken, parameters.update);

	// Delete a item with id
  app.post('/sys/parameter/delete', VerifyToken, parameters.delete);
  //#endregion
}