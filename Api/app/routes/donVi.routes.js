module.exports = app => {
  const donVi = require("../controllers/donVi.controller");
  const VerifyToken = require("../controllers/VerifyToken");
  
  // Get tree donvi
  app.post("/donvis/gettree", VerifyToken, donVi.getTree);

  app.post("/donvis/gettree-value", VerifyToken, donVi.getTree_Value);

  // Get tree hiển trị trên treeTable
  app.post("/donvis/gettree-node", VerifyToken, donVi.getTreeNode);
};
