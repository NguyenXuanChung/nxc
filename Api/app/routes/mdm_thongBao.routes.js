const verifyToken = require('../controllers/VerifyToken');
const Common = require('../models/common.model');

module.exports = (app) => {
	const VerifyToken = require('../controllers/VerifyToken');
	const cheDoPhats = require('../controllers/mdm_thongBao/dm_cheDoPhat.controller');
	const kenhs = require('../controllers/mdm_thongBao/dm_kenh.controller');
	const kieuLaps = require('../controllers/mdm_thongBao/dm_kieuLap.controller');
	const kieuPhats = require('../controllers/mdm_thongBao/dm_kieuPhat.controller');
	const loaiThongBaos = require('../controllers/mdm_thongBao/dm_loaiThongBao.controller');
	const loaiTins = require('../controllers/mdm_thongBao/dm_loaiTin.controller');
	const trangThaiDuyets = require('../controllers/mdm_thongBao/dm_trangThaiDuyet.controller');
	const trangThaiPhats = require('../controllers/mdm_thongBao/dm_trangThaiPhat.controller');
	const tb_files = require('../controllers/mdm_thongBao/thongBao_file.controller');
	const tb_thietBis = require('../controllers/mdm_thongBao/thongBao_thietBi.controller');
	const thongBaos = require('../controllers/mdm_thongBao/thongBao.controller');

	//#region DM Chế độ phát
	// Retrieve data with condition in body
	app.post('/mdm/dm-chedophat/get', VerifyToken, cheDoPhats.get);

	// Create a new item
	app.post('/mdm/dm-chedophat/insert', VerifyToken, cheDoPhats.create);

	// Update a item with id
	app.post('/mdm/dm-chedophat/update', VerifyToken, cheDoPhats.update);

	// Delete a item with id
	app.post('/mdm/dm-chedophat/delete', VerifyToken, cheDoPhats.delete);
	//#endregion

	//#region DM Kênh
	// Retrieve data with condition in body
	app.post('/mdm/dm-kenh/get', VerifyToken, kenhs.get);

	// Create a new item
	app.post('/mdm/dm-kenh/insert', VerifyToken, kenhs.create);

	// Update a item with id
	app.post('/mdm/dm-kenh/update', VerifyToken, kenhs.update);

	// Delete a item with id
	app.post('/mdm/dm-kenh/delete', VerifyToken, kenhs.delete);
	//#endregion

	//#region DM Kiểu lặp
	// Retrieve data with condition in body
	app.post('/mdm/dm-kieulap/get', VerifyToken, kieuLaps.get);

	// Create a new item
	app.post('/mdm/dm-kieulap/insert', VerifyToken, kieuLaps.create);

	// Update a item with id
	app.post('/mdm/dm-kieulap/update', VerifyToken, kieuLaps.update);

	// Delete a item with id
	app.post('/mdm/dm-kieulap/delete', VerifyToken, kieuLaps.delete);
	//#endregion

	//#region DM Kiểu phát
	// Retrieve data with condition in body
	app.post('/mdm/dm-kieuphat/get', VerifyToken, kieuPhats.get);

	// Create a new item
	app.post('/mdm/dm-kieuphat/insert', VerifyToken, kieuPhats.create);

	// Update a item with id
	app.post('/mdm/dm-kieuphat/update', VerifyToken, kieuPhats.update);

	// Delete a item with id
	app.post('/mdm/dm-kieuphat/delete', VerifyToken, kieuPhats.delete);
	//#endregion

	//#region DM Loại thông báo
	// Retrieve data with condition in body
	app.post('/mdm/dm-loaithongbao/get', VerifyToken, loaiThongBaos.get);

	// Create a new item
	app.post('/mdm/dm-loaithongbao/insert', VerifyToken, loaiThongBaos.create);

	// Update a item with id
	app.post('/mdm/dm-loaithongbao/update', VerifyToken, loaiThongBaos.update);

	// Delete a item with id
	app.post('/mdm/dm-loaithongbao/delete', VerifyToken, loaiThongBaos.delete);
	//#endregion

	//#region DM Loại tin
	// Retrieve data with condition in body
	app.post('/mdm/dm-loaitin/get', VerifyToken, loaiTins.get);

	// Create a new item
	app.post('/mdm/dm-loaitin/insert', VerifyToken, loaiTins.create);

	// Update a item with id
	app.post('/mdm/dm-loaitin/update', VerifyToken, loaiTins.update);

	// Delete a item with id
	app.post('/mdm/dm-loaitin/delete', VerifyToken, loaiTins.delete);
	//#endregion

	//#region DM Trạng thái duyệt
	// Retrieve data with condition in body
	app.post('/mdm/dm-trangthaiduyet/get', VerifyToken, trangThaiDuyets.get);

	// Create a new item
	app.post('/mdm/dm-trangthaiduyet/insert', VerifyToken, trangThaiDuyets.create);

	// Update a item with id
	app.post('/mdm/dm-trangthaiduyet/update', VerifyToken, trangThaiDuyets.update);

	// Delete a item with id
	app.post('/mdm/dm-trangthaiduyet/delete', VerifyToken, trangThaiDuyets.delete);
	//#endregion

	//#region DM Trạng thái phát
	// Retrieve data with condition in body
	app.post('/mdm/dm-trangthaiphat/get', VerifyToken, trangThaiPhats.get);

	// Create a new item
	app.post('/mdm/dm-trangthaiphat/insert', VerifyToken, trangThaiPhats.create);

	// Update a item with id
	app.post('/mdm/dm-trangthaiphat/update', VerifyToken, trangThaiPhats.update);

	// Delete a item with id
	app.post('/mdm/dm-trangthaiphat/delete', VerifyToken, trangThaiPhats.delete);
	//#endregion

	//#region Thiết bị thông báo
	// Retrieve data with condition in body
	app.post('/mdm/thongbao-thietbi/get', VerifyToken, tb_thietBis.get);

	// Create a new item
	app.post('/mdm/thongbao-thietbi/insert', VerifyToken, tb_thietBis.create);

	// Update a item with id
	app.post('/mdm/thongbao-thietbi/update', VerifyToken, tb_thietBis.update);

	// Delete a item with id
	app.post('/mdm/thongbao-thietbi/delete', VerifyToken, tb_thietBis.delete);
	//#endregion

	//#region File thông báo
	// Retrieve data with condition in body
	app.post('/mdm/thongbao-file/get', VerifyToken, tb_files.get);

	// Create a new item
	app.post('/mdm/thongbao-file/insert', VerifyToken, tb_files.create);

	// Update a item with id
	app.post('/mdm/thongbao-file/update', VerifyToken, tb_files.update);

	// Delete a item with id
	app.post('/mdm/thongbao-file/delete', VerifyToken, tb_files.delete);
	//#endregion

	//#region Thông báo
	// Retrieve data with condition in body
	app.post('/mdm/thongbao/get', VerifyToken, thongBaos.get);

	app.post('/mdm/thongbao/get-list', VerifyToken, thongBaos.getList);

	app.post('/mdm/thongbao/get-t2s', VerifyToken, thongBaos.getT2S);

	app.post('/mdm/thongbao/get-by-diaban', VerifyToken, thongBaos.getByDiaBan);

	// Create a new item
	app.post('/mdm/thongbao/insert', VerifyToken, thongBaos.create);

	app.post('/mdm/thongbao/duyet/get', VerifyToken, thongBaos.getDuyet);
	app.post('/mdm/thongbao/duyet/insert', VerifyToken, thongBaos.createDuyet);
	app.post('/mdm/thongbao/duyet/update', VerifyToken, thongBaos.updateDuyet);

	// Update a item with id
	app.post('/mdm/thongbao/update', VerifyToken, thongBaos.update);

	// Delete a item with id
	app.post('/mdm/thongbao/delete', VerifyToken, thongBaos.delete);
	//#endregion

	//#region Audio
	app.get('/mdm/stream-audio', verifyToken, Common.streamingAudio);

	app.get('/mdm/stream-audio/:file', (req, res) => {
		req.body = { ...req.body, filePath: `./uploads/audio/${req.params.file}` };
		Common.streamingAudio(req, res);
	});

	app.get('/mdm/download/:id/:filename', (req, res) => {
		req.body = { id: req.params.id, filename: req.params.filename };
		thongBaos.download(req, res);
	});

	app.get('/mdm/streaming/:id/:filename', (req, res) => {
		req.body = { id: req.params.id, filename: req.params.filename };
		thongBaos.streaming(req, res);
	});

	app.get('/mdm/lichphat/:serial', (req, res) => {
		req.body = { serial: req.params.serial };
		thongBaos.getLichPhatByDevice(req, res);
	});
	//#endregion
};
